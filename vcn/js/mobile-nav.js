jQuery(document).ready(function(){
	jQuery("#nav .mobile").click(function(){
		jQuery(".menu-primary li").slideToggle();
	});
	
	jQuery(window).resize(mobileResize);
	
	mobileResize();
});

function mobileResize(){
	if(jQuery(window).width() > 600){
		jQuery(".menu-primary li").removeAttr("style");
		jQuery(".menu-primary .sub-menu").removeAttr("style");
		jQuery(".menu-primary .menu-item-has-children i").remove();
	} else {
		if ( jQuery( ".menu-primary .menu-item-has-children i" ).length == 0 ) {
			jQuery('<i class="fas fa-chevron-down fa-lg"></i>').insertAfter(
				jQuery(".menu-primary .menu-item-has-children > a")
			);
			jQuery(".menu-primary .menu-item-has-children i").click(function(){
				self = jQuery(this);
				if (self.next().is(":hidden")){
					self.removeClass("fa-chevron-down");
					self.addClass("fa-chevron-up");
				} else {
					self.removeClass("fa-chevron-up");
					self.addClass("fa-chevron-down");
				}
				self.next().slideToggle();
			});
		}
	}
}
