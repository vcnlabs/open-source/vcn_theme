<?php
// Official Genesis code snippets: https://my.studiopress.com/documentation/snippets/

// Start the engine ====================================================

// allow search form in text or widgets
require_once( get_template_directory() . '/lib/init.php' );

// add search shortcode
add_shortcode('wpbsearch', 'get_search_form');

// add log in form shortcode
function login_url_shortcode( $attrs = array (), $content = '' ) {
	$a = shortcode_atts( array(
		'user' => '',
	), $attrs );
	
	$theme = wp_login_form(array('value_username' => $a['user']));
	
	return $theme;
}
add_shortcode('loginform', 'login_url_shortcode');

// Header ==============================================================

/** Make the website responsive. */
add_action( 'wp_head', 'responsive_meta' );
function responsive_meta() 
{
    echo '<meta name="viewport" content="width=device-width" />';
} 

/** Set the header image, but remove background logo added by genesis*/
add_theme_support( 'genesis-custom-header', array( 
	'width' => 330, 'height' => 100, 
	'header_callback' => 'header_text'
));
function header_text(){}

/** remove and add an custom theme header */
remove_action('genesis_header','genesis_do_header');
add_action('genesis_header','header_layout');
function header_layout() { ?>
<header id="title-area">
  	<div id="logo"><a href="<?php echo home_url()?>" alt="Home Page">
		<img src="<?php echo get_header_image()?>" />
	</a>
	</div>
	
	<?php if ( is_active_sidebar( 'header-right' ) ) : ?>
		<div id="header-widgets" class="header-widgets">
			<div class="wrap">
				<div class="widget-area header-widget header-widget-area">
					<?php dynamic_sidebar( 'header-right' ); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</header>
<?php }

/** adds the mobile button */
add_filter( 'wp_nav_menu_items', 'add_mobile_navigation', 10, 2 );
function add_mobile_navigation($menu, $args){
	$menu = "<div class='mobile'><i class='fas fa-bars'></i><span>MENU</span></div>" . $menu;
	return $menu;
}

add_action( 'wp_enqueue_scripts', 'add_mobile_script' );
function add_mobile_script() {
	$path = ( is_child_theme() ? get_stylesheet_directory_uri() : get_template_directory_uri() ) .
		'js/mobile-nav.js';
	wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/mobile-nav.js', array ( 'jquery' ) );
}

// Remove Unused Customization =====================================

/** Removes the secondary menu in Appearence > Menus */
add_theme_support( 'genesis-menus', array( 'primary' => __( 'Primary Navigation Menu', 'genesis' ) ) );

/** Removes the secondary menu in Appearence > Widgets */
unregister_sidebar( 'sidebar-alt' );

/** Removes other unused widgets (from ) */
add_action( 'widgets_init', 'unregister_genesis_widgets', 20 );
function unregister_genesis_widgets() {
	unregister_widget( 'Genesis_eNews_Updates' );
	unregister_widget( 'Genesis_Featured_Page' );
	unregister_widget( 'Genesis_Featured_Post' );
	unregister_widget( 'Genesis_Latest_Tweets_Widget' );
	unregister_widget( 'Genesis_Menu_Pages_Widget' );
	unregister_widget( 'Genesis_User_Profile_Widget' );
	unregister_widget( 'Genesis_Widget_Menu_Categories' );
}

// Breadcrumb ==========================================================

/** Customize breadcrub text */
add_filter( 'genesis_breadcrumb_args', 'breadcrumb_text' );
function breadcrumb_text( $args ) {
	 // Removes the "You are here: "
  	$args['labels']['prefix'] = ''; 
	
	// Replaces '/' with '»'
	$args['sep'] = '&raquo;'; 
  	return $args;
}

/** Removes breadcrumb from pages without a parent. */
add_action('genesis_before', 'breadcrumb_before');
function breadcrumb_before(){
	global $post;
	if (is_page() && $post->post_parent == 0){
		remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );
	}
}

/** Removes home text and link*/
add_filter ( 'genesis_home_crumb', 'breadcrumb_home' ); 
function breadcrumb_home(){
	/// Remove breadcrumb home text
	 $crumb = '';
     return $crumb;
}

// Post Content ==========================================================
// 
add_action ( 'genesis_post_title', 'category_name', 9 );
function category_name() {
/*
	if ( ! is_singular('post') )
		return;*/
 
    $categories = get_the_category();

	if ( empty($categories ) ) return;
        
 	echo "<h3 class='categories'>";
    $out = "";
    foreach ($categories as $category){
    	if (! empty($out) ){
        	$out .= " ";
        }
    	$out .= $category->name;
    }
	echo $out;
    echo "</h3>";
}

add_filter( 'genesis_post_info', 'author_post_info' );
function author_post_info($post_info) {
	if ( !is_page() ) {
		$post_info = 'Posted [post_date] by [post_author]. [post_edit]';
		return $post_info;
    }
}

/** Remove the post category at the end of the page **/
remove_action( 'genesis_after_post_content', 'genesis_post_meta');

/// Footer =================================================================================

/* Remove back to top link */
add_filter( 'genesis_footer_backtotop_text', 'back_to_top' );
function back_to_top($backtotop) {
    $backtotop = '';
    return $backtotop;
}
		   
/* Creates the four footer widgets */
add_theme_support( 'genesis-footer-widgets', 4 );

/** Replace the footer credit*/
add_filter('genesis_footer_creds_text', 'credit_footer');
function credit_footer( $creds ) {
	$creds = '[footer_copyright] Vancouver Community Network.';
	return $creds;
}
?>
