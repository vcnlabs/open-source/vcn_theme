Install instruction [here](files/README.md)

# Folder Structure

* [download](downloads): Folder for all downloads
* [download/child](downloads/child): Folder for sample child theme
* [download/genesis](downloads/genesis): Folder for the genesis themework
* [download/website](downloads/website): Folder for VCN website
* [files](files): Folder for images to add to the media library, or for html for Custom HTML widgets:
* [vcn](vcn): Folder for the theme, to install, compress the folder into a zip file and upload

# Plugin used

* [All-in-One Event Calendar](https://wordpress.org/plugins/all-in-one-event-calendar/):
  Plugin for events
* [Business Directory Plugin](https://wordpress.org/plugins/business-directory-plugin/):
  plugin for directory
* [Collapse-O-Matic](https://wordpress.org/plugins/jquery-collapse-o-matic/): 
  Used for Mail login and search at the top of the page
* [Menu Image](https://wordpress.org/plugins/menu-image/): Home icon at 
  the navigation area
* [WP Font Awesome](https://wordpress.org/plugins/wp-font-awesome/):
  Facebook and Twitter icons
* [Slide Anything – Responsive Content / HTML Slider and Carousel](https://en-ca.wordpress.org/plugins/slide-anything/):
  Plugin for the silder and carousel on the home page.
* [Delightful Downloads](https://wordpress.org/plugins/delightful-downloads/):
  plugin for links
* [List category posts](https://wordpress.org/plugins/list-category-posts/):
  plugin for short code for the news section of the front page

# [Images](vcn/images) in the themes usage

|Image                                            |Usage                       |
|-------------------------------------------------|----------------------------|
|![capi-base.png](vcn/images/capyi-base.png)      |Non-hover image for capyi   |
|![capyi-hover](vcn/images/capyi-hover.png)       |Hover image for capyi       |  
|![donation-base.png](vcn/images/donate-base.png) |Non-hover image for donation|   
|![donation-hover](vcn/images/donate-hover.png)   |Hover image for donation    |
|![submenu.png](vcn/images/submenu.png)           |Indicate sub menu           |
|![thispage.png](vcn/images/thispage.png)         |Indicate this page at menu  |
|![favicon.ico](vcn/images/favicon.ico)           |Default icon                |
|![logo.png](vcn/images/header.png)               |Default logo                |
|![screenshot.png](vcn/screenshot.png)            |Theme image                 |
|![home-card-1](vcn/images/home-card-services.jpg)|Home page card image        |
|![home-card-2](vcn/images/home-card-calendar.jpg)|Home page card image        |
|![home-card-3](images/home-card-newsletter.png)  |Home page card image        |
|![home-card-4](images/home-card-events.jpg)      |Home page card image        |
|![home-card-5](images/home-card-internet.jpg)    |Home page card image        |
|![home-card-6](images/home-card-news.jpg)        |Home page card image        |

