# Steps to take to install file

## Before switching themes

1. turn off comments, which should be done in the main website:
	1. set all comments to unapproved 
	2. turn off everything in discussion settings
	3. uncheck all `allow comments` post/article in quick edit
2. Install/keep the following plugins:
	* All-in-One Event Calendar
	* Business Directory Plugin
	* Menu Image
	* Delightful Downloads
	* Collapse-O-Matic
	* WP Font Awesome
	* Slide Anything – Responsive Content / HTML Slider and Carousel
	* List category posts
3. Create the [top sliders](top-slider.md) and the 
   [bottom carsusel](carsusel.md). Take a note the their short code.
4. Create a new [home page](../files/home.html). The text at the top and 
   at the bottom needs to be updated
5. Remove the title from the pages that is repeat in the main content
	* There are the main contents not matching the title of the text
	* 
6. Replace the content on [this page](https://www2.vcn.bc.ca/community-events/)
   to [this](../files/calendar.html)

## Switching themes
1. add the theme [Genesis](downloads/genesis.2.6.1.zip)
2. compress the [vcn](vcn) folder, this become the theme to upload
3. activate vcn theme

# After switching themes
1. Add the new [widgets](widgets.md). This can be made before switching
   but it need to sort into the right area after switching
